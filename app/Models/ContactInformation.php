<?php

namespace App\Models;

use App\Events\ContactInformationAdded;

/**
 * @property int id,
 * @property string firstname
 * @property string lastname
 * @property string street
 * @property string housenumber
 * @property string zip
 * @property string city
 * @property string email
 */
class ContactInformation extends BaseModel
{
    protected $table = 'contact_informations';

    protected $fillable = [
        'firstname',
        'lastname',
        'street',
        'housenumber',
        'zip',
        'city',
        'email',
    ];

    public static function boot()
    {
        // when a new contact information is added, a corresponding event is fired
        static::created(function (ContactInformation $contactInformation) {
            ContactInformationAdded::dispatch($contactInformation);
        });

        parent::boot();
    }
}
