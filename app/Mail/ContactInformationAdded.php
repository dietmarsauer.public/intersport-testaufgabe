<?php

namespace App\Mail;

use App\Models\ContactInformation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Queue\SerializesModels;

/**
 * E-Mail notification to admin on new contact information added.
 */
class ContactInformationAdded extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(public ContactInformation $contactInformation)
    {
    }

    public function content(): Content
    {
        return new Content(
            view: 'emails.contact-information-added',
        );
    }
}
