<?php

namespace App\Listeners;

use App\Events\ContactInformationAdded as ContactInformationAddedEvent;
use App\Mail\ContactInformationAdded as ContactInformationAddedMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * Listener on new contact information to send email notification.
 */
class SendContactInformationNotification
{
    public string $toEmail;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->toEmail = env('ADMIN_EMAIL');
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ContactInformationAdded  $event
     * @return void
     */
    public function handle(ContactInformationAddedEvent $event)
    {
        Log::info(sprintf('execute contact information added event: %d',$event->contactInformation->id));

        Mail::to($this->toEmail)->send(new ContactInformationAddedMail($event->contactInformation));
    }
}
