<?php

namespace App\Http\Controllers;

use App\Models\ContactInformation;
use Illuminate\Http\Request;

class ContactFormController extends Controller
{
    /**
     * Save contact information and send back positive signal.
     *
     * @param Request $request
     * @return string
     */
    public function send(Request $request): string
    {
        $validated = $request->validate([
            'firstname' => 'required|max:100',
            'lastname' => 'required|max:100',
            'street' => 'nullable|max:60',
            'housenumber' => 'nullable|max:16',
            'zip' => 'integer|nullable|digits:5',
            'city' => 'nullable|max:50',
            'email' => 'email:rfc|required|max:320',
        ]);

        // not included in requirements, but maybe useful
//        $contactInformation = ContactInformation::where('email', $validated['email'])->firstOrNew();
        $contactInformation = new ContactInformation();
        $contactInformation->fill($validated);
        $contactInformation->save();

        return response()->json(['success' => true]);
    }
}
