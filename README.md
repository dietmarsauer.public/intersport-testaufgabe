# INTERSPORT Testaufgabe - Dietmar Sauer

## Feature overview

* Unter http://localhost/ befindet sich ein Formular zur Eingabe von Kontaktinformationen.
* Gewisse Felder sind Pflichtangaben.
* Beim Absenden wird die Eingabe validiert und auf Fehler entsprechend hingewiesen.
* Sind keine Fehler vorhanden, werden die Kontaktinformationen in einer Datenbank gespeichert.
* Der Admin wird hierüber informiert.

## Getting Started

### Install

Benutze git clone, um das Repository zu klonen.

```
git clone https://gitlab.com/dietmarsauer.public/intersport-testaufgabe.git
```

Um das Projekt zu starten, führe `docker-compose up` aus.

Außerdem muss das Frontend gestartet werden per `npm run dev`.

Initial muss die Datenbank per `php artisan migrate` gefüllt werden.

### requirements

Create a Form where a User can add his Contact Information’s:

    - Firstname*

    - Lastname*

    - Street

    - Housenumber

    - Zip Code

    - City

    - Email*

    * = Required Fields

Save the Datasets in the Database via AXIOS Rest API CALL from The User Interface.

Send a simple Email Notification after the dataset saved in the Database.



Technologies to use:

Laravel 9

Vue.JS

AXIOS CALL for frontend

Postgres > 14 or MongoDB >= 5

Docker Compose to Start the Project



Notes: Please upload your Project to a not restricted git Repository.

